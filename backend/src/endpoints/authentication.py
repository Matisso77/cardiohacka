from flask import make_response
from flask_login import login_required, login_user, current_user, logout_user
import bcrypt
from db import DAO

def register(body):
    email = body.get('email')
    password = body.get('password')
    username = body.get('username')
    role = body.get('role')
    
    # Password hashing
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(password.encode(), salt)

    # Check data uniqueness 
    if DAO.getUserData(username):
        return make_response('Username already in use', 400)
    try:
        DAO.addUser(email, username, hashed, role) 
        return make_response('Registration successful', 200)
    except Exception as e:
        return make_response(f'Database error: {e}', 500)

def login(body):
    username = body.get('username')
    password = body.get('password')

    user = DAO.getUserData(username)
    
    if user is not None and bcrypt.checkpw(password.encode(), user.password):
        login_user(user, remember=True)
        return make_response('', 200)
    return make_response('Login failed', 401)

@login_required
def logout():
    logout_user()
    return make_response('', 200)

def isLogged():
    if current_user.is_authenticated:
        return make_response('', 200)    
    return make_response('', 401)