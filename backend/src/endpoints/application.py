from flask import make_response
from flask_login import login_required, login_user, current_user, logout_user
from db import DAO
import json

@login_required
def addMeasurements(body):
    measurements = body.get('measurements')
    try:
        for measurement in measurements:  
            DAO.updateUserMeasurement(current_user.username, measurement)
        return make_response('Update successful', 200)
    except Exception as e:
        return make_response(f'Database error: {e}', 500)

@login_required
def getMeasurements(body): #TODO: positional argument not supplied
    username = current_user.username
    start = body.get('start')
    end = body.get('end')
    try:
        measurements = DAO.getUserMeasurement(username, start, end)
        res = json.dumps(measurements) #TODO: Database error: Object of type Cursor is not JSON serializable
        return make_response(res, 200) 
    except Exception as e:
        return make_response(f'Database error: {e}', 500)

@login_required
def updateDoctors(body):
    username = current_user.username
    doctors = body #TODO: possibly change
    try:
        DAO.updateDoctorList(username, doctors)
        return make_response('Update successful', 200)
    except Exception as e:
        return make_response(f'Database error: {e}', 500)

@login_required
def getDoctors():
    username = current_user.username
    try:
        doctors = DAO.getDoctorList(username)
        res = json.dumps(doctors)
        return make_response(res, 200) 
    except Exception as e:
        return make_response(f'Database error: {e}', 500)
    
@login_required
def getNotifications():
    username = current_user.username
    pass #TODO: implement