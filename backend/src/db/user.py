from flask_login import UserMixin

class User(UserMixin):
    def __init__(self, user_json):
        self.user_json = user_json
        self.username = self.user_json.get('username')
        self.password = self.user_json.get('password')
        self.email = self.user_json.get('email')
        self.role = self.user_json.get('role')
        self.doctorList = self.user_json.get('doctorList')

    def get_id(self):
        return self.username