from random import randint
from pymongo import MongoClient
from .user import User

client = MongoClient("mongodb+srv://pyClient:KLoe3nC3TpK1rwsG@cluster0-jhmtv.mongodb.net/test?retryWrites=true&w=majority")
db=client.cardio

##Input simple data
def generateSimpleData():
    names = ['Konrad','Gabe','Bartek', 'Kuba', 'Mati']
    lastName = ['B','T','S', 'C', 'Y']
    password = ['Pizza', 'Bar Food', 'Fast Food', 'Italian', 'Mexican', 'American', 'Sushi Bar', 'Vegetarian']
    for _ in range(1, 10):
        users = {
            'email' : names[randint(0, (len(names)-1))] + ' ' + lastName[randint(0, (len(lastName)-1))],
            'username' : names[randint(0, (len(names)-1))]+ ' ' + password[randint(0, (len(password)-1))],
            'password' : password[randint(0, (len(password)-1))],
            'role': "doctor",
            'doctorList': []
        }
        db.users.insert_one(users)

##Template: db.CollectionName.find_one({"field":parameter})
def getUserData(username):
    user_dict = db.users.find_one({'username': username})
    if user_dict:
        return User(user_dict)
    return None

##Template: db.CollectionName.insert_one({"field":parameter}) or (jsonObject)
def addUser(email, username, password, role):
    if(db.users.find_one({'username': username})==None and db.users.find_one({'email': email})==None):
        user = {
                'email' : email,
                'username' : username,
                'password' : password,
                'role' : role
            }
        db.users.insert_one(user)
    else:
        raise Exception('Registration failed')

def getUserMeasurement(username, start, end):
    if(db.users.find_one({'username': username})!=None):
        user_dict = db.users.find_one({'username': username})
        return db.measurement.find({'userId': user_dict.get('_id'), "date":{"$gt": start}, "date":{"$lt": end}})
        # return db.measurement.find({'userId': user_dict.get('_id'), "date":{"$gt": start, "$lt": end}}) #TODO: replace above line when it fails
    else:
        raise Exception(f'There is no user {username}')
    
def updateUserMeasurement(username, measurement):
    if(db.users.find_one({'username': username})!=None):
        user_dict = db.users.find_one({'username': username})
        measurement['userId'] = user_dict.get('_id')
        db.measurement.insert_one(measurement)
    else:
        raise Exception(f'There is no user {username}')

def updateDoctorList(username, doctorList):
    if(db.users.find_one({'username': username})!=None):
        myquery = { "username": username}
        newvalues = { "$set":   { "doctorList": doctorList }}
        user_dict = db.users.update_one(myquery, newvalues)
    else:
        raise Exception(f'There is no user {username}')

def getDoctorList(username):
    user_dict = db.users.find_one({'username': username})
    return user_dict.get('doctorList')