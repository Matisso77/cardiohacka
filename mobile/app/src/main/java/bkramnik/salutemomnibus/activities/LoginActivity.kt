package bkramnik.salutemomnibus.activities

import android.content.Intent
import android.os.Bundle
import android.support.annotation.VisibleForTesting
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import bkramnik.salutemomnibus.R
import bkramnik.salutemomnibus.service.DataService
import bkramnik.salutemomnibus.service.MyGcmListenerService
import bkramnik.salutemomnibus.service.MyInstanceIDListenerService
import bkramnik.salutemomnibus.service.RegistrationIntentService
import com.android.volley.Response
import com.android.volley.VolleyError
import org.json.JSONException
import org.json.JSONObject

class LoginActivity() : AppCompatActivity(), View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {

    lateinit var loginButton: Button
    lateinit var loginTextView: EditText
    lateinit var passwordTextView: EditText
    lateinit var progressBar: ProgressBar
    private lateinit var dataService: DataService

    @VisibleForTesting
    constructor(service: DataService) : this() {
        this.dataService = service
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginButton = findViewById(R.id.loginButton)
        loginTextView = findViewById(R.id.loginTextView)
        passwordTextView = findViewById(R.id.passwordTextView)
        progressBar = findViewById(R.id.progressBar)
        dataService = DataService.getDataService(this)


        loginButton.setOnClickListener(this)
        startService(Intent(this, RegistrationIntentService::class.java))
        startService(Intent(this, MyInstanceIDListenerService::class.java))
        startService(Intent(this, MyGcmListenerService::class.java))

    }

    override fun onClick(v: View?) {
        if (loginTextView.text.isBlank() || passwordTextView.text.isBlank()) {
            showError(R.string.fillBothTextView)
        } else {
            showLoading()
            dataService.getAuthToken(loginTextView.text.toString(), passwordTextView.text.toString(), this, this)
        }
    }

    override fun onResponse(response: JSONObject?) {
        try {
            val token = resolveToken(response)
            if (token.isBlank()) {
                Log.d("HTTPError", "Empty Token!!!")
                showError(R.string.logingError)
            } else {
                dataService.setAuthToken(token)
                dataService.setUserId(resolveUserId(response))
                goToMainActivity()
            }
        } catch (e: JSONException) {
            Log.d("JSONError", e.message)
            showError(R.string.logingError)
        } finally {
            hideLoading()
        }
    }

    private fun resolveToken(response: JSONObject?) = response!!.getString("access_token")

    private fun resolveUserId(response: JSONObject?) = response!!.getLong("userID")

    private fun goToMainActivity() {
        runOnUiThread {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onErrorResponse(error: VolleyError?) {
        if(error?.networkResponse?.statusCode == 401){
            showError(R.string.wrongPass)
            Log.d("error", error.toString())
        }
        else{
            showError(R.string.logingError)
            Log.d("error", error.toString())
        }
        hideLoading()
    }

    private fun showLoading() {
        runOnUiThread {
            progressBar.visibility = View.VISIBLE
            loginButton.visibility = View.INVISIBLE
            loginTextView.visibility = View.INVISIBLE
            passwordTextView.visibility = View.INVISIBLE
        }

    }

    private fun hideLoading() {
        runOnUiThread {
            progressBar.visibility = View.INVISIBLE
            loginButton.visibility = View.VISIBLE
            loginTextView.visibility = View.VISIBLE
            passwordTextView.visibility = View.VISIBLE
        }
    }

    private fun showError(stringId: Int) {
        runOnUiThread {
            Toast.makeText(this, resources.getString(stringId), Toast.LENGTH_LONG).show()
        }
    }
}
