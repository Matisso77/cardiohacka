package bkramnik.salutemomnibus.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import bkramnik.salutemomnibus.R;
import bkramnik.salutemomnibus.model.CompleteMeasure;
import bkramnik.salutemomnibus.model.SimpleMeasure;
import bkramnik.salutemomnibus.service.BluetoothHeartRateAdapter;
import bkramnik.salutemomnibus.service.DataService;

import java.util.ArrayList;
import java.util.List;

public class BluetoothMeasureDialog extends DialogFragment implements BluetoothHeartRateAdapter.DevicesListListener, BluetoothHeartRateAdapter.HeartRateListener, AdapterView.OnItemClickListener {

    private ListView listView;
    private ProgressBar progressBar;
    private ArrayAdapter<String> adapter;
    private BluetoothHeartRateAdapter bluetoothAdapter;
    private List<String> foundDevicesMacs = new ArrayList<>();
    private List<String> foundDevicesNames = new ArrayList<>();
    private DataService dataService;
    private AlertDialog alertDialog;
    private boolean disableCallbacks = false;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.get_pulse_miband_dialog, null);
        listView = view.findViewById(R.id.listView);
        progressBar = view.findViewById(R.id.progressBar2);
        adapter = new ArrayAdapter<>(getContext(), R.layout.list_view_cell, R.id.textView2);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        bluetoothAdapter = new BluetoothHeartRateAdapter(this.getContext());
        dataService = DataService.getDataService(this.getContext());

        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setView(view);
        builder.setNegativeButton(R.string.close, (dialog, which) -> dismiss());
        alertDialog = builder.create();

        showLoading();
        bluetoothAdapter.getAvailableDevices(this, this.getContext());

        return alertDialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disableCallbacks = true;
    }

    private void showLoading() {
        if (!disableCallbacks) {
            getActivity().runOnUiThread(() -> {
                listView.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
            });
        }

    }

    private void showList() {
        if (!disableCallbacks) {
            getActivity().runOnUiThread(() -> {
                adapter.notifyDataSetChanged();
                listView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            });
        }

    }

    private void showText() {
        if (!disableCallbacks) {
            getActivity().runOnUiThread(() -> {
                listView.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            });
        }
    }

    @Override
    public void devicesFound(String deviceMac, String name) {
        foundDevicesNames.add(name);
        foundDevicesMacs.add(deviceMac);
        adapter.add(name);
        showList();
    }

    @Override
    public void scanCompleted() {
        showList();
    }

    @Override
    public void scanIngError() {
        if (!disableCallbacks) {
            showToast(R.string.baseError);
            this.dismiss();
        }
    }

    @Override
    public void heartRateRead(Integer value) {
        if (!disableCallbacks) {
            this.getActivity().runOnUiThread(
                    () -> dataService.sendMeasurements(CompleteMeasure.of(new SimpleMeasure(1, 1, 1, value, "Pomiar pobrany automatycznie")),
                            response -> {
                                showText();
                                showToast(R.string.measurementSent);
                                this.dismiss();
                            },
                            error -> {
                                showToast(R.string.baseError);
                                Log.d("error:", error.getMessage());
                            }));
        }
    }

    @Override
    public void glucoseRead(Integer value) {
        if (!disableCallbacks) {
            this.getActivity().runOnUiThread(
                    () -> dataService.sendMeasurements(CompleteMeasure.of(new SimpleMeasure(1, 2, 1, value, "Pomiar pobrany automatycznie")),
                            response -> {
                                showText();
                                showToast(R.string.measurementSent);
                                this.dismiss();
                            },
                            error -> {
                                showToast(R.string.baseError);
                                Log.d("error:", error.getMessage());
                            }));
        }
    }

    @Override
    public void bloodPressureRead(Integer value) {
        if (!disableCallbacks) {
            this.getActivity().runOnUiThread(
                    () -> dataService.sendMeasurements(CompleteMeasure.of(new SimpleMeasure(1, 3, 1, value, "Pomiar pobrany automatycznie")),
                            response -> {
                                showText();
                                showToast(R.string.measurementSent);
                                this.dismiss();
                            },
                            error -> {
                                showToast(R.string.baseError);
                                Log.d("error:", error.getMessage());
                            }));
        }
    }

    @Override
    public void heartRateReadingError() {
        showList();
        showToast(R.string.heartRateReadingError);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        showLoading();
        bluetoothAdapter.getHartRateFromDevice(foundDevicesMacs.get(position), this);
    }

    private void showToast(int id) {
        this.getActivity().runOnUiThread(() -> Toast.makeText(this.getContext(), id, Toast.LENGTH_SHORT).show());
    }
}
