package bkramnik.salutemomnibus.activities

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.opengl.Visibility
import android.os.Bundle
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import bkramnik.salutemomnibus.R
import bkramnik.salutemomnibus.model.CompleteMeasure
import bkramnik.salutemomnibus.model.MeasureType
import bkramnik.salutemomnibus.model.SimpleMeasure
import bkramnik.salutemomnibus.service.DataService
import bkramnik.salutemomnibus.service.JsonConverter
import java.util.stream.Collectors


class MeasureDialog : DialogFragment() {

    companion object {
        const val DEVICE_ID = 1
    }

    private lateinit var measureTextView: TextView
    private lateinit var unitTextView: TextView
    private lateinit var valueTextView: TextView
    private lateinit var descTextView: TextView
    private lateinit var measureSpinner: Spinner
    private lateinit var unitSpinner: Spinner
    private lateinit var valueET: EditText
    private lateinit var descET: EditText
    private lateinit var progressBar: ProgressBar

    private lateinit var service: DataService
    private val measure = CompleteMeasure()
    private lateinit var measuresAdapter: ArrayAdapter<String>
    private lateinit var unitsAdapter: ArrayAdapter<String>
    private lateinit var measureTypesList : List<MeasureType>

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = activity.layoutInflater.inflate(R.layout.add_measure_dialog, null)

        val builder = AlertDialog.Builder(activity)
        builder.setView(view)
        builder.setNeutralButton(R.string.addNextMeasure, null)
        builder.setPositiveButton(R.string.send, null)

        setUpWidgets(view)
        showLoading()
        service = DataService.getDataService(this.context)

        getMeasureTypes()
        setFindingUnitsOnSelectingMeasureType()

        val dialog = builder.create()
        setButtonsBehavior(dialog)

        return dialog
    }

    private fun getMeasureTypes() {
        service.getMeasureTypes({
            this.measureTypesList = JsonConverter.getMeasureTypes(it)
            measuresAdapter.addAll(getmeasureNames(this.measureTypesList))
            measuresAdapter.notifyDataSetChanged()
            hideLoading()
        }, {
            showError(R.string.baseError)
            this.dismiss()
        })
    }

    private fun setFindingUnitsOnSelectingMeasureType() {
        measureSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                showLoading()
                setUnits(position)
            }
        }
    }

    private fun setButtonsBehavior(dialog: AlertDialog) {
        dialog.setOnShowListener {
            val button = dialog.getButton(AlertDialog.BUTTON_NEUTRAL)
            button.setOnClickListener {
                if (measureSpinner.selectedItem == null || unitSpinner.selectedItem == null || valueET.text.isBlank()) {
                    showError(R.string.errorMeasureNotFill)
                } else {
                    measure.addMeasure(SimpleMeasure(DEVICE_ID, getMeasureIdFromPosition(measureSpinner.selectedItemPosition),
                        getUnitIdFromPosition(measureSpinner.selectedItemPosition, unitSpinner.selectedItemPosition),
                        valueET.text.toString().toDouble(),
                        descET.text.toString()))
                    clearFields()
                }
            }

            val button2 = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            button2.setOnClickListener {
                if (measureSpinner.selectedItem == null || unitSpinner.selectedItem == null || valueET.text.isBlank()) {
                    showError(R.string.errorMeasureNotFill)
                } else {
                    showLoading()
                    measure.addMeasure(SimpleMeasure(DEVICE_ID, getMeasureIdFromPosition(measureSpinner.selectedItemPosition),
                        getUnitIdFromPosition(measureSpinner.selectedItemPosition, unitSpinner.selectedItemPosition),
                        valueET.text.toString().toDouble(),
                        descET.text.toString()))
                    service.sendMeasurements(measure, {
                        showError(R.string.measureSent)
                        this.dismiss()
                    }, {
                        showError(R.string.baseError)
                    })
                }
            }
        }
    }

    private fun setUpWidgets(view: View) {
        measureTextView = view.findViewById(R.id.measureTV)
        unitTextView = view.findViewById(R.id.unitTV)
        descTextView = view.findViewById(R.id.descTV)
        valueTextView = view.findViewById(R.id.valueTV)
        measureSpinner = view.findViewById(R.id.measureSpinner)
        unitSpinner = view.findViewById(R.id.unitSpinner)
        valueET = view.findViewById(R.id.measureValueET)
        descET = view.findViewById(R.id.measureDescET)
        progressBar = view.findViewById(R.id.progressBar)
        measuresAdapter = ArrayAdapter(this.context, R.layout.support_simple_spinner_dropdown_item)
        measureSpinner.adapter = measuresAdapter
        unitsAdapter = ArrayAdapter(this.context, R.layout.support_simple_spinner_dropdown_item)
        unitSpinner.adapter = unitsAdapter
    }


    private fun clearFields() {
        activity.runOnUiThread {
            measureSpinner.prompt = ""
            unitsAdapter.clear()
            unitsAdapter.notifyDataSetChanged()
            valueET.text.clear()
            descET.text.clear()
        }
    }

    private fun   setUnits(measureNumber: Int){
        unitsAdapter.clear()
        unitsAdapter.addAll(getUnitNames(measureNumber))
        unitsAdapter.notifyDataSetChanged()
        hideLoading()
    }

    private fun getUnitNames(measureNumber: Int) : List<String>{

        return measureTypesList[measureNumber].units.stream()
            .map { unit -> unit.name }
            .collect(Collectors.toList())
    }

    private fun showLoading() {
        activity.runOnUiThread {
            measureTextView.visibility = View.GONE
            unitTextView.visibility = View.GONE
            descTextView.visibility = View.GONE
            valueTextView.visibility = View.GONE
            measureSpinner.visibility = View.GONE
            unitSpinner.visibility = View.GONE
            valueET.visibility = View.GONE
            descET.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        }
    }

    private fun hideLoading() {
        activity.runOnUiThread {
            measureTextView.visibility = View.VISIBLE
            unitTextView.visibility = View.VISIBLE
            descTextView.visibility = View.VISIBLE
            valueTextView.visibility = View.VISIBLE
            measureSpinner.visibility = View.VISIBLE
            unitSpinner.visibility = View.VISIBLE
            valueET.visibility = View.VISIBLE
            descET.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }

    private fun showError(stringId: Int) {
        activity.runOnUiThread {
            Toast.makeText(this.context, resources.getString(stringId), Toast.LENGTH_LONG).show()
        }
    }

    private fun getmeasureNames(measureTypes : List<MeasureType>) : List<String>{
        val result = ArrayList<String>(measureTypes.size)
        measureTypes.forEach {
            result.add(it.name)
        }
        return result
    }

    private fun getMeasureIdFromPosition(position: Int): Int{
        return measureTypesList[position].id
    }

    private fun getUnitIdFromPosition(positionMeasure: Int, positionUnit: Int): Int{
        return measureTypesList[positionMeasure].getUnit(positionUnit).id
    }
}