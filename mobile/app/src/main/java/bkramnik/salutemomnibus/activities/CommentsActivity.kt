package bkramnik.salutemomnibus.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.Toast
import bkramnik.salutemomnibus.R
import bkramnik.salutemomnibus.model.CommentsListAdapter
import bkramnik.salutemomnibus.service.DataService
import bkramnik.salutemomnibus.service.JsonConverter
import com.android.volley.Response
import com.android.volley.VolleyError
import org.json.JSONArray

class CommentsActivity : AppCompatActivity(), Response.ErrorListener, Response.Listener<JSONArray> {

    private lateinit var dataService: DataService
    private lateinit var commentsLV: ListView
    private lateinit var progressBar: ProgressBar
    private lateinit var adapter: CommentsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)

        commentsLV = findViewById(R.id.commentsLV)
        progressBar = findViewById(R.id.progressBar4)

        dataService = DataService.getDataService(this)


    }

    override fun onResume() {
        super.onResume()
        showLoading()
        dataService.getComments(this, this)
    }

    override fun onResponse(response: JSONArray?) {
        val comments = JsonConverter.getComments(response)
        adapter = CommentsListAdapter(comments, this)
        commentsLV.adapter = adapter
        adapter.notifyDataSetChanged()
        hideLoading()
    }

    override fun onErrorResponse(error: VolleyError?) {
        Log.d("error:", error.toString())
        showToast(R.string.baseError)
    }

    private fun showLoading() {
        runOnUiThread {
            commentsLV.visibility = View.INVISIBLE
            progressBar.visibility = View.VISIBLE
        }
    }

    private fun hideLoading() {
        runOnUiThread {
            commentsLV.visibility = View.VISIBLE
            progressBar.visibility = View.INVISIBLE
        }
    }

    private fun showToast(stringId: Int) {
        runOnUiThread {
            Toast.makeText(this, resources.getString(stringId), Toast.LENGTH_LONG).show()
        }
    }
}
