package bkramnik.salutemomnibus.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageButton
import android.widget.Toast
import bkramnik.salutemomnibus.R


class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        if (v!!.id == R.id.addMeasureButton) {
            runOnUiThread {
                val measureDialog = MeasureDialog()
                measureDialog.show(fragmentManager, "Dodaj pomiary")
            }
        }
        if (v.id == R.id.comments) {
            runOnUiThread {
                startActivity(Intent(this, CommentsActivity::class.java))
            }
        }
        if (v.id == R.id.miBandDialog) {
            if (checkIfAlreadyHavePermission()) {
                runOnUiThread {

                    val miBandDialog = BluetoothMeasureDialog()
                    miBandDialog.show(fragmentManager, "Pobierz pomiary")
                }
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ),
                    101
                )
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            101 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                runOnUiThread {
                    val miBandDialog = BluetoothMeasureDialog()
                    miBandDialog.show(fragmentManager, "Pobierz pomiary")
                }
            } else {
                runOnUiThread {
                    Toast.makeText(this, R.string.permissionError, Toast.LENGTH_LONG).show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun checkIfAlreadyHavePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private lateinit var measureButton: ImageButton
    private lateinit var miBandButton: ImageButton
    private lateinit var commentButton: ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        measureButton = findViewById(R.id.addMeasureButton)
        measureButton.setOnClickListener(this)

        miBandButton = findViewById(R.id.miBandDialog)
        miBandButton.setOnClickListener(this)

        commentButton = findViewById(R.id.comments)
        commentButton.setOnClickListener(this)
    }
}
