package bkramnik.salutemomnibus.model;

public class SimpleMeasure {
    private int deviceID;
    private int name;
    private int unit;
    private double value;
    private String description = "";

    public SimpleMeasure(int deviceID, int name, int unit, double value, String description) {
        this.deviceID = deviceID;
        this.name = name;
        this.unit = unit;
        this.value = value;
        this.description = description;
    }

    public SimpleMeasure() {
    }

    public SimpleMeasure(int deviceID, int name, int unit, double value) {
        this.deviceID = deviceID;
        this.name = name;
        this.unit = unit;
        this.value = value;
        this.description ="";
    }

    public int getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(int deviceID) {
        this.deviceID = deviceID;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
