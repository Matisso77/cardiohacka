package bkramnik.salutemomnibus.model;

import android.icu.text.SimpleDateFormat;
import android.util.Log;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public class Comment {

    SimpleDateFormat oldFormat =  new SimpleDateFormat("EE, dd MMM yyyy HH:mm:ss z",
            Locale.ENGLISH);
    SimpleDateFormat formatterNew = new SimpleDateFormat("dd.MMM - HH:mm", Locale.ENGLISH);

    private long commentId;
    private String content;
    private long measurementID;
    private String timestamp;

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getMeasurementID() {
        return measurementID;
    }

    public void setMeasurementID(long measurementID) {
        this.measurementID = measurementID;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {

        try {
            Date date = oldFormat.parse(timestamp);
            this.timestamp = formatterNew.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("error parsing date:", e.getMessage());
        }
    }
}
