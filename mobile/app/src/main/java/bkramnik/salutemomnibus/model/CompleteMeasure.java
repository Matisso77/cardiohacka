package bkramnik.salutemomnibus.model;

import android.icu.util.Calendar;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CompleteMeasure {

    private String datetime;
    private List<SimpleMeasure> measurements;

    public static CompleteMeasure of(SimpleMeasure measure){
        CompleteMeasure completeMeasure = new CompleteMeasure();
        completeMeasure.addMeasure(measure);
        return completeMeasure;
    }

    public CompleteMeasure() {
        measurements = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        datetime = sdf.format(Calendar.getInstance().getTime());
    }

    public void addMeasure(SimpleMeasure simpleMeasure) {
        measurements.add(simpleMeasure);
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public List<SimpleMeasure> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<SimpleMeasure> measurements) {
        this.measurements = measurements;
    }

    public String getJson(Gson gson) {
        return gson.toJson(this);
    }

    public String getJson() {
        return (new Gson()).toJson(this);
    }
}
