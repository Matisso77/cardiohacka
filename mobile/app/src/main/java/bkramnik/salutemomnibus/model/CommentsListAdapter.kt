package bkramnik.salutemomnibus.model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import bkramnik.salutemomnibus.R

class CommentsListAdapter(var data: List<Comment>, var context: Context) : BaseAdapter() {

    private var inflater: LayoutInflater? = null


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        if (inflater == null) inflater = LayoutInflater.from(context)

        if (convertView == null) {
            val view = inflater!!.inflate(R.layout.comment_cell, null)
            val dateTv = view.findViewById<TextView>(R.id.measureDateTv)
            val commentTv = view.findViewById<TextView>(R.id.commentTV)

            dateTv.text = data[position].timestamp
            commentTv.text = data[position].content

            val viewHolder = ViewHolder(dateTv, commentTv)
            view.tag = viewHolder
            return view
        } else {
            val viewHolder = convertView.tag as ViewHolder
            viewHolder.dateTV.text = data[position].timestamp
            viewHolder.commentTv.text = data[position].content
            return convertView
        }


    }

    override fun getItem(position: Int): Any {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data.size
    }

    class ViewHolder(val dateTV: TextView, val commentTv: TextView)
}