package bkramnik.salutemomnibus.model;

public class HeartRateMeasure extends SimpleMeasure {

    private static int nameHeartRate = 1;
    private static int unit = 1;

    public HeartRateMeasure(int deviceID,  double value, String description) {
        super(deviceID, nameHeartRate, unit, value, description);
    }
}
