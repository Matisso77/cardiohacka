package bkramnik.salutemomnibus.model;

import java.util.ArrayList;
import java.util.List;

public class MeasureType {

    private String name;
    private List<Unit> units;
    private int id;

    public MeasureType(String name, int id, List<Unit> units) {
        this.name = name;
        this.units = units;
        this.id = id;
    }

    public MeasureType(String name, int id) {
        units = new ArrayList<>();
        this.name = name;
        this.id = id;
    }

    public MeasureType() {
        units = new ArrayList<>();
        name ="";
    }

    public List<Unit> getUnits() {
        return units;
    }

    public void setUnits(List<Unit> units) {
        this.units = units;
    }

    public Unit getUnit(int id) {
        return units.get(id);
    }

    public void addUnit(Unit unit) {
        this.units.add(unit);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
