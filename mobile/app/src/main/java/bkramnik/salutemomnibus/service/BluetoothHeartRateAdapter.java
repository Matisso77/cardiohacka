package bkramnik.salutemomnibus.service;

import android.content.Context;
import android.util.Log;
import androidx.annotation.VisibleForTesting;
import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.RxBleDevice;
import com.polidea.rxandroidble2.scan.ScanResult;
import com.polidea.rxandroidble2.scan.ScanSettings;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

import java.nio.ByteBuffer;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BluetoothHeartRateAdapter {

    private static final long BT_UUID_LOWER_BITS = 0x800000805F9B34FBl;
    private static final long BT_UUID_UPPER_BITS = 0x1000l;
    private static final int UUID_HEART_RATE = 0x2A37;
    private static final int UUID_BLOOD_PRESSURE = 0x2A35;
    private static final int UUID_GLUCOSE = 0x2A18;

    private RxBleClient rxBleClient;

    public BluetoothHeartRateAdapter(Context context) {
        this.rxBleClient = RxBleClient.create(context);
    }

    @VisibleForTesting
    public BluetoothHeartRateAdapter(RxBleClient client) {
        this.rxBleClient = client;
    }

    public void getAvailableDevices(DevicesListListener listener, Context context) {
        rxBleClient.scanBleDevices(new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build()).distinct(scanResult -> scanResult.getBleDevice().getMacAddress())
                .filter(scanResult -> scanResult.getBleDevice().getName() != null)
                .filter(scanResult -> !"XX:XX:XX:XX:XX:XX".equals(scanResult.getBleDevice().getMacAddress()))
                .subscribe(new Observer<ScanResult>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ScanResult scanResult) {
                        listener.devicesFound(scanResult.getBleDevice().getMacAddress(), scanResult.getBleDevice().getName());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("error", e.getMessage());
                        listener.scanIngError();
                    }

                    @Override
                    public void onComplete() {
                        listener.scanCompleted();
                    }
                });
        listener.devicesFound("asdasd", "Pedometer");
        listener.devicesFound("asdasd", "Huawei Watch X");
    }

    public void getHartRateFromDevice(String mac, HeartRateListener listener) {
        RxBleDevice bleDevice = rxBleClient.getBleDevice(mac);

        bleDevice.establishConnection(true)
                .timeout(15, TimeUnit.SECONDS)
                .subscribe(rxBleConnection ->{
                            rxBleConnection.readCharacteristic(get16BitBTUUID(UUID_HEART_RATE)).subscribe(new SingleObserver<byte[]>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onSuccess(byte[] bytes) {
                                    try {
                                        ByteBuffer wrapped = ByteBuffer.wrap(bytes);
                                        listener.heartRateRead(wrapped.getInt());
                                    } catch (Exception e) {
                                        Log.d("error", e.getMessage());
                                        listener.heartRateReadingError();
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.d("error", e.getMessage());
                                    listener.heartRateReadingError();
                                }
                            });

                            rxBleConnection.readCharacteristic(get16BitBTUUID(UUID_BLOOD_PRESSURE)).subscribe(new SingleObserver<byte[]>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onSuccess(byte[] bytes) {
                                    try {
                                        ByteBuffer wrapped = ByteBuffer.wrap(bytes);
                                        listener.bloodPressureRead(wrapped.getInt());
                                    } catch (Exception e) {
                                        Log.d("error", e.getMessage());
                                        listener.heartRateReadingError();
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.d("error", e.getMessage());
                                    listener.heartRateReadingError();
                                }
                            });

                            rxBleConnection.readCharacteristic(get16BitBTUUID(UUID_GLUCOSE)).subscribe(new SingleObserver<byte[]>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onSuccess(byte[] bytes) {
                                    try {
                                        ByteBuffer wrapped = ByteBuffer.wrap(bytes);
                                        listener.glucoseRead(wrapped.getInt());
                                    } catch (Exception e) {
                                        Log.d("error", e.getMessage());
                                        listener.heartRateReadingError();
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.d("error", e.getMessage());
                                    listener.heartRateReadingError();
                                }
                            });
                        },
                        throwable -> {
                            Log.d("error", throwable.getMessage());
                            listener.heartRateReadingError();
                        }
                );
    }

    private UUID get16BitBTUUID(long uuid) {
        return new UUID(BT_UUID_UPPER_BITS + (uuid << 32), BT_UUID_LOWER_BITS);
    }

    public interface DevicesListListener {
        void devicesFound(String deviceMac, String name);

        void scanCompleted();

        void scanIngError();
    }

    public interface HeartRateListener {
        void heartRateRead(Integer value);

        void glucoseRead(Integer value);

        void bloodPressureRead(Integer value);

        void heartRateReadingError();
    }
}
