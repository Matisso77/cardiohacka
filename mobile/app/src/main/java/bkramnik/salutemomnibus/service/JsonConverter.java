package bkramnik.salutemomnibus.service;


import bkramnik.salutemomnibus.model.Comment;
import bkramnik.salutemomnibus.model.MeasureType;
import bkramnik.salutemomnibus.model.Unit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonConverter {

    public static List<MeasureType> getMeasureTypes(JSONArray json) throws JSONException {

        List<MeasureType> result = new ArrayList<>(json.length());

        for (int i = 0; i < json.length(); i++) {
            JSONObject measureJson = json.getJSONObject(i);
            MeasureType measureType = new MeasureType(measureJson.getString("name"), measureJson.getInt("id"));
            JSONArray unitsArray = measureJson.getJSONArray("units");
            for (int j = 0; j < unitsArray.length(); j++) {
                measureType.addUnit(new Unit(unitsArray.getJSONObject(j).getString("name"), unitsArray.getJSONObject(j).getInt("id")));
            }
            result.add(measureType);
        }
        return result;
    }

    public static List<Comment> getComments(JSONArray json) throws JSONException {
        List<Comment> result = new ArrayList<>(json.length());
        for (int i = 0; i < json.length(); i++) {
            JSONObject commentJson = json.getJSONObject(i);
            Comment comment = new Comment();
            comment.setCommentId(commentJson.getLong("commentID"));
            comment.setMeasurementID(commentJson.getLong("measurementID"));
            comment.setContent(commentJson.getString("content"));
            comment.setTimestamp(commentJson.getString("timestamp"));
            result.add(comment);
        }
        return result;
    }

}
