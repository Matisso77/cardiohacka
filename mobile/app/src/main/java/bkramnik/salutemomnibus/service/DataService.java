package bkramnik.salutemomnibus.service;

import android.content.Context;
import android.util.Log;
import bkramnik.salutemomnibus.R;
import bkramnik.salutemomnibus.model.CompleteMeasure;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DataService {

    private static DataService dataServiceSingleton;


    public static DataService getDataService(Context context) {
        if (dataServiceSingleton == null) {
            dataServiceSingleton = new DataService(context);
        }
        return dataServiceSingleton;
    }

    private String baseUrl;
    private RequestQueue queue;
    private String authToken;
    private String gcmToken;
    private Long userId;

    private DataService(Context context) {
        queue = Volley.newRequestQueue(context);
        baseUrl = context.getResources().getString(R.string.baseUrl);
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public void getAuthToken(String login, String password, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {

        JSONObject params = new JSONObject();
        try {
            params.put("email", login);
            params.put("password", password);
            params.put("notification_key", gcmToken);
        } catch (JSONException e) {
            Log.d("error", e.getMessage());
            e.printStackTrace();
        }

        Log.d("params dla logowania:", params.toString());
        String url = baseUrl + "auth/login";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params, responseListener, errorListener);
        queue.add(jsonObjectRequest);
    }

    public void setAuthToken(String authToken) {
        this.authToken = "JWT " + authToken;
    }

    public void setGcmToken(String gcmToken) {
        this.gcmToken = gcmToken;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void getMeasureTypes(Response.Listener<JSONArray> responseListener, Response.ErrorListener errorListener) {
        String url = baseUrl + "measurements/types";
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.GET, url, null, responseListener, errorListener);
        queue.add(jsonObjectRequest);

    }


    public void sendMeasurements(CompleteMeasure completeMeasure, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {

        try {
            JSONObject params = new JSONObject((new Gson()).toJson(completeMeasure));
            Log.d("Json dla pomiarów:", params.toString());
            String url = baseUrl + "measurements/add";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params, responseListener, errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", authToken);
                    return headers;
                }
            };
            queue.add(jsonObjectRequest);
        } catch (JSONException e) {
            Log.d("error", e.getMessage());
            e.printStackTrace();
        }

    }

    public void getComments(Response.Listener<JSONArray> responseListener, Response.ErrorListener errorListener) {
        try {
            responseListener.onResponse(new JSONArray("[\n" +
                    "    {\n" +
                    "        \"commentID\": 1,\n" +
                    "        \"content\": \"zbyt wysokie ciśnienie, proszę się umówić na wizytę\",\n" +
                    "        \"measurementID\": 1,\n" +
                    "        \"timestamp\": \"Wed, 21 Feb 2018 01:23:45 GMT\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "        \"commentID\": 2,\n" +
                    "        \"content\": \"pomiary w normie\",\n" +
                    "        \"measurementID\": 2,\n" +
                    "        \"timestamp\": \"Wed, 21 Feb 2018 01:23:45 GMT\"\n" +
                    "    }\n" +
                    "]\n"));
            return;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = baseUrl + "comments/user/" + userId;
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.GET, url, null, responseListener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", authToken);
                return headers;
            }
        };
        queue.add(jsonObjectRequest);

    }


}
