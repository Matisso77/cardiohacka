package bkramnik.salutemomnibus;

import android.support.test.runner.AndroidJUnit4;
import bkramnik.salutemomnibus.service.BluetoothHeartRateAdapter;
import bkramnik.salutemomnibus.service.BluetoothHeartRateAdapter.DevicesListListener;
import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.RxBleConnection;
import com.polidea.rxandroidble2.RxBleDevice;
import com.polidea.rxandroidble2.scan.ScanResult;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
public class BluetoothAdapterTest {

    public static final String DEVICE_1 = "Device1";
    public static final String MAC1 = "111:111:111";

    @Test
    public void shouldFindAvailableDevice() {
        ScanResult scanResult = Mockito.mock(ScanResult.class);
        RxBleDevice rxBleDevice = Mockito.mock(RxBleDevice.class);
        Mockito.when(scanResult.getBleDevice()).thenReturn(rxBleDevice);
        Mockito.when(rxBleDevice.getMacAddress()).thenReturn(MAC1);
        Mockito.when(rxBleDevice.getName()).thenReturn(DEVICE_1);

        RxBleClient rxBleClient = Mockito.mock(RxBleClient.class);
        when(rxBleClient.scanBleDevices(Mockito.any(), Mockito.any())).thenReturn(Observable.just(scanResult));

        BluetoothHeartRateAdapter adapter = new BluetoothHeartRateAdapter(rxBleClient);

        adapter.getAvailableDevices(new DevicesListListener() {
            @Override
            public void devicesFound(String deviceMac, String name) {
                assertEquals(deviceMac, MAC1);
                assertEquals(name, DEVICE_1);
            }

            @Override
            public void scanCompleted() {

            }

            @Override
            public void scanIngError() {
                Assert.fail();
            }
        }, null);
    }

    @Test
    public void shouldFindHeartRate() {
        RxBleClient rxBleClient = Mockito.mock(RxBleClient.class);
        RxBleDevice device = Mockito.mock(RxBleDevice.class);
        RxBleConnection rxBleConnection = Mockito.mock(RxBleConnection.class);
        when(rxBleConnection.readCharacteristic((UUID) Mockito.any())).thenReturn(new Single<byte[]>() {
            @Override
            protected void subscribeActual(SingleObserver<? super byte[]> observer) {
                observer.onSuccess(new byte[]{0, 0, 0, 0, 0, 0, 0, 4});
            }
        });
        when(device.establishConnection(Mockito.any())).thenReturn(Observable.just(rxBleConnection))
        when(rxBleClient.getBleDevice(Mockito.any())).thenReturn(device);

        BluetoothHeartRateAdapter adapter = new BluetoothHeartRateAdapter(rxBleClient);

        adapter.getHartRateFromDevice(MAC1, new BluetoothHeartRateAdapter.HeartRateListener() {
            @Override
            public void heartRateRead(Integer value) {
                assertEquals(4, value.intValue());
            }

            @Override
            public void glucoseRead(Integer value) {
                Assert.fail();
            }

            @Override
            public void bloodPressureRead(Integer value) {
                Assert.fail();
            }

            @Override
            public void heartRateReadingError() {
                Assert.fail();
            }
        });
    }
}
