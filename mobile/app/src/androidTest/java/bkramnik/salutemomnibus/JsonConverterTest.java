package bkramnik.salutemomnibus;

import android.support.test.runner.AndroidJUnit4;
import bkramnik.salutemomnibus.model.Comment;
import bkramnik.salutemomnibus.model.MeasureType;
import bkramnik.salutemomnibus.service.JsonConverter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class JsonConverterTest {

    public static final String COMMENT_ID = "commentID";
    public static final String MEASUREMENT_ID = "measurementID";
    public static final String CONTENT = "content";
    public static final String TIMESTAMP = "timestamp";

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEWhenNullObject() throws JSONException {
        JsonConverter.getComments(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPE2WhenNullObject() throws JSONException {
        JsonConverter.getMeasureTypes(null);
    }

    @Test
    public void shouldReturnCorrectCommentWhenCorrectObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(COMMENT_ID, 1);
        jsonObject.put(MEASUREMENT_ID, 1);
        jsonObject.put(CONTENT, "contenttttt");
        jsonObject.put(TIMESTAMP, "Thu, 06 Jun 2019 14:51:41 GMT");

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(jsonObject);

        List<Comment> comments = JsonConverter.getComments(jsonArray);
        Comment comment = comments.get(0);

        assertEquals(comment.getCommentId(), 1);
        assertEquals(comment.getContent(), "contenttttt");
        assertEquals(comment.getMeasurementID(), 1);
        assertEquals(comment.getTimestamp(), "06.Jun - 14:51");
    }

    @Test
    public void shouldReturnCorrectMeasureTypesWhenCorrectRequest() throws JSONException {
        JSONObject unitsObject = new JSONObject();
        unitsObject.put("name", "BP");
        unitsObject.put("id", 1);

        JSONObject measureObject = new JSONObject();
        JSONArray unitsArray = new JSONArray();
        unitsArray.put(unitsObject);

        measureObject.put("units", unitsArray);
        measureObject.put("name", "HeartRate");
        measureObject.put("id", 1);

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(measureObject);

        List<MeasureType> measureTypes = JsonConverter.getMeasureTypes(jsonArray);
        MeasureType measureType = measureTypes.get(0);

        assertEquals(measureType.getId(), 1);
        assertEquals(measureType.getName(), "HeartRate");
        assertEquals(measureType.getUnits().get(0).getId(), 1);
        assertEquals(measureType.getUnits().get(0).getName(), "BP");
    }
}
