# CardioHacka

Backend

## Uruchamianie
Prerekwizyty: python3 (polecam 3.8), pip
`pip install -r requirements.txt`
Są tam biblioteki, które ostatecznie nie będą użyte, ale to nieistotne, potem posprzątam.

Potem robimy `flask run` z katalogu `backend/src`. Serwer będzie nasłuchiwał pod adresem `127.0.0.1:5000`
Pod url `127.0.0.1:5000/ui` znajduje się dokumentacja swaggera.

Poniżej tej linii na razie nic nie zmieniałem
-------------------------------
## Endpoints

### **Autoryzacja**
* POST http://127.0.0.1:3000/application/auth/register

    ```
    {
	    "email" : "test_",
	    "password" : "pass",
	    "first_name" : "Jan",
	    "surname" : "xxx",
	    "national_id" : "aaa1111",
	    "telephone" : "1234567"
    }
    ```
    
    Rejestruje użytkownika

* POST http://127.0.0.1:3000/application/auth/login
    
    ```
    {
        "email": "test_",
        "password": "pass"
        "notification_key":"AASd23r2rsff..."
    }
    ```
    notification_key - klucz do powiadomien gcm
    Zalogowuje użytkownika i zwraca:

    ```
    {
        "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NTgzNjU4MDAsInN1YiI6OSwibmJmIjoxNTU4MzY1ODAwLCJleHAiOjE1NTgzNjg4MDB9.fuNkamyeOd_ksJGYa1GCaWR9sOrZGDCfef7AXmgKu74",
        "message": "You logged in successfully.",
        "role": 1,
        "userID": 9
    }
    ```

### **Pomiary**

* GET http://127.0.0.1:3000/application/measurements/types
    
    Zwraca dostępne typy pomiarów wraz z jednostkami

* POST http://127.0.0.1:3000/application/measurements/add

    **Headers:**

    ```Authorization: JWT TOKEN```

    ```Content-Type: application/json```

    **Body:**
    ```
    {
        "datetime":"2018-01-23T01:23:45.123456Z",
        "measurements": [
            { "deviceID":1, "name":1, "unit":2, "value":76.0, "description":"" },
            { "deviceID":1, "name":2, "unit":2, "value":120.0, "description":"" },
            { "deviceID":1, "name":3, "unit":1, "value":80.0, "description":"" }
        ]
    }
    ```
    Dodaje pomiar,
    gdzie 
    * name to id nazwy pomiaru (np. name:1 to pomiar ciśnienia -> id możemy uzyskać z endpointu measurements/types)
    * unit to id nazwy jednostki (jak wyżej)


* GET http://127.0.0.1:3000/application/measurements/user_id

    Zwraca id typów pomiarów, które posiada użytkownik.

    ```
    [
        {
            "id": 1,
            "name": "Cisnienie skurczowe"
        },
        {
            "id": 2,
            "name": "Cisnienie rozkurczowe"
        },
        {
            "id": 3,
            "name": "Tętno"
        }
    ]
    ```


* GET http://127.0.0.1:3000/application/measurements/1?type=1&date_from=2015-01-23T01:23:45.123456Z&date_to=2018-02-23T01:23:45.123456Z

    **Parametry:**
    * type - id typu pomiaru
    * date_from - data pierwszego pomiaru
    * date_to - data ostatniego pomiaru

    ```
    [
        {
            "description": "",
            "deviceID": 1,
            "id": 1,
            "timestamp": "Tue, 23 Jan 2018 01:23:45 GMT",
            "typeID": 1,
            "typeName": "Cisnienie skurczowe",
            "unitID": 2,
            "unitName": "Stopnie Celsiusza",
            "value": 76
        },
        {
            "description": "",
            "deviceID": 1,
            "id": 4,
            "timestamp": "Fri, 23 Feb 2018 01:23:45 GMT",
            "typeID": 1,
            "typeName": "Cisnienie skurczowe",
            "unitID": 2,
            "unitName": "Stopnie Celsiusza",
            "value": 86
        }
    ]
    ```
    Zwraca pomiary z danego przedziału czasowego

### **Użytkownicy**
* GET ```http://127.0.0.1:3000/application/users```
    
    Zwraca wszystkich użytkowników

* GET ```http://127.0.0.1:3000/application/users/<id_użytkownika>```
    
    Zwraca informacje o danym użytkowniku
    
    ```
    {
        "email": "adam",
        "firstName": "Jan",
        "id": 1,
        "nationalID": "12dwx1",
        "roleID": 2,
        "surname": "xxx",
        "telephone": "1234567"
    }
    ```  

* POST ```http://127.0.0.1:3000/application/user```

    ```
    {
        “user_id”: 123,
        “email” : ‘adam’,
        “password” : ‘hasloooo’,
        “first_name” : ‘Jan’,
        “surname” : ‘xxx’,
        “national_id”: ‘12dwx1’,
        “telephone” : ‘1234567’,
        “role_id” : 3
    }
    ```
    Edytuje użytkownika
    Konieczne jest podanie jedynie parametru user_id, reszta parametrów jest opcjonalna, tzn. jeśli podamy tylko “surname” : “Stonoga” to zmieni się jedynie nazwisko użytkownika. Nie możemy zmienić user_id.


### **Doktorzy:**

* GET ```http://127.0.0.1:3000/application/doctors```

   zwraca wszystkich doktorów


* GET ```http://127.0.0.1:3000/application/doctors/<id_doktora>```

   zwraca wszystkich pacjentów danego doktora

* POST ```http://127.0.0.1:3000/application/doctors/<id_doktora>/patient/<id_pacjenta>```

   dodaje pacjenta do danego doktora (id_doktora i id_pacjenta jest tym samym co id usera)

* DELETE  ```http://127.0.0.1:3000/application/doctors/<id_doktora>/patient/<id_pacjenta>```

   usuwa pacjenta od doktora 


### **Urządzenia:**

* POST ```http://127.0.0.1:3000/application/devices/```

   ```
   {
        "deviceType": "holterEKG",
	    "producer" : "Omron",
	    "model" : "xD11",
	    "serialNumber" : "8234xD",
	    "others" : ""
   }
   ```
   dodaje nowe urządzenie

* GET ```http://127.0.0.1:3000/application/devices/```

   zwraca wszystkie urządzenia

* PATCH ```http://127.0.0.1:3000/application/devices/<id_urządzenia>```

    ```
    {
        "deviceType": "holterSRKG",
        "producer" : "Omron",
        "model" : "xD11",
        "serialNumber" : "8234xD",
        "others" : "",
        "userID" : 3
    }
    ```
    aktualizuje informacje o urządzeniu (aktualizuje tylko te pola, które są wrzucone do jsona, tzn. jeśli chcemy zaktualizować tylko mode, to w jsona możemy wrzucić jedynie pole model)

* DELETE ```http://127.0.0.1:3000/application/devices/<id_urządzenia>```

    usuwa urządzenie

### **Komentarze:**

* POST ```http://127.0.0.1:3000/application/comments/user/<id_uzytkownika>```

   ```
    {
        "timestamp":"2018-02-21T01:23:45.123456Z",
        "measurementID":2,
        "content": "dupsdfa"
    }
   ```
   dodaje nowy komentarz

* GET ```http://127.0.0.1:3000/application/comments/user/<id_uzytkownika>```

   zwraca 100 ostatnich komentarzy danego użytkownika

    ```
    [
        {
            "commentID": 1,
            "content": "dupa",
            "measurementID": 1,
            "timestamp": "Wed, 21 Feb 2018 01:23:45 GMT"
        },
        {
            "commentID": 2,
            "content": "dupsdfa",
            "measurementID": 2,
            "timestamp": "Wed, 21 Feb 2018 01:23:45 GMT"
        }
    ]
    ```

* PATCH ```http://127.0.0.1:3000/application/comments/<id_komentarza>```

    ```
    {
        "content":"asdfdf"
    }
    ```
    edytuje treść komentarza

* DELETE ```http://127.0.0.1:3000/application/comments/<id_komentarza>```

    usuwa komentarz


##Api do dodawania pomiarów

Zapytanie typu POST na ```/measurements/add``` w headerze ma być token tak jak niżej:
KEY`Authorization`, VALUE`JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE1NTM3MTQxMzQsImlhdCI6MTU1MzcxNDEzNCwiZXhwIjoxNTUzNzE0NDM0LCJzdWIiOjN9.w7UnJuBDkyLykV-eN_cHcBNGYHaMDQVhn9bOfIK4WZg`
 * A w body ma być json taki jak niżej:

```{
  "datetime":"2018-01-23T01:23:45.123456Z",
  "measurements": [
    { "deviceID":1, "name":"pulse", "unit":"bpm", "value":76.0, "description":"" },
    { "deviceID":1, "name":"systolic BP", "unit":"mmHg", "value":120.0, "description":"" },
    { "deviceID":1, "name":"diastolic BP", "unit":"mmHg", "value":80.0, "description":"" }
  ]
 }```
 
 Uwaga! Przed uruchomieniem koniecznie jest komenda make db/migrate && make db/upgrade
 
 Zapytanie typu POST na ```/measurements/add``` w headerze ma być token tak jak niżej:
KEY`Authorization`, VALUE`JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE1NTM3MTQxMzQsImlhdCI6MTU1MzcxNDEzNCwiZXhwIjoxNTUzNzE0NDM0LCJzdWIiOjN9.w7UnJuBDkyLykV-eN_cHcBNGYHaMDQVhn9bOfIK4WZg`
 * A w body ma być json taki jak niżej:

```{
  "datetime":"2018-01-23T01:23:45.123456Z",
  "measurements": [
    { "deviceID":1, "name":"pulse", "unit":"bpm", "value":76.0, "description":"" },
    { "deviceID":1, "name":"systolic BP", "unit":"mmHg", "value":120.0, "description":"" },
    { "deviceID":1, "name":"diastolic BP", "unit":"mmHg", "value":80.0, "description":"" }
  ]
 }```
 
 
## Wystawianie API do logowanie

Przykładowa rejestracja: POST ```http://127.0.0.1:3000/application/auth/register?email=user12a&password=dupa1234```
Gdy ok. zwraca ```201``` i body:

```
{
    "message": "You registered successfully. Please log in."
}
```

Gdy nok np. bo użytkownik istnieje ```202``` i body:
```
{
    "message": "User already exists. Please login."
}
```

Przykładowe logowanie: POST ```http://127.0.0.1:3000/application/auth/login?email=user12a&password=dupa1234```

Gdy ok zwraca ```200`` i body:
```
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTM2NDgwOTAsInN1YiI6MywiaWF0IjoxNTUzNjQ3NzkwfQ.gsEsyQMZdK6gJ6FAeLAqJ2bhf7c5zLD1ouEK5JlrVm4",
    "message": "You logged in successfully."
}
```

Gdy złe hasło bądź login zwraca ```401```
i body:
```
{
    "message": "Invalid email or password, Please try again"
}
```

Jak uruchomić? Pobrać repo, przełączyć się na branch feature/login, w katalogu głównym ```make install```, a potem ```make start```. Jak nie zadziała to z sudo.


Przykładowy route z wymaganym zalogowaniem: 
```http://127.0.0.1:3000/application/measurements```
I w headerze trzeba wrzucić KEY```Authorization``` z VALUE```JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE1NTM3MTQxMzQsImlhdCI6MTU1MzcxNDEzNCwiZXhwIjoxNTUzNzE0NDM0LCJzdWIiOjN9.w7UnJuBDkyLykV-eN_cHcBNGYHaMDQVhn9bOfIK4WZg```



##Research urządzeń medycznych z otwartym API

http://developer.withings.com/oauth2/
https://omronhealthcare.com/api/
https://developer.dexcom.com/overview
https://www.programmableweb.com/category/medical/api
https://developer.ihealthlabs.com/index.html
