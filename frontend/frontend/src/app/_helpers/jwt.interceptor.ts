import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '@app/_services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let currentUser = this.authenticationService.currentUserValue;
        console.log("interceptor");
        if (currentUser && currentUser.access_token) {
            console.log("interceptor:" + currentUser.access_token)
            request = request.clone({
                setHeaders: { 
                    'Authorization': `JWT ${currentUser.access_token}`,
                    'Content-Type': 'application/json'
                }
            });
        }
        return next.handle(request);
    }
}