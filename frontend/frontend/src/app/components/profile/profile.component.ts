import { Component, OnInit } from '@angular/core';
import { AuthenticationService, UserService } from '@app/_services';
import { User } from '@app/_models';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public currentUser: User;
  public userProfile: any;
  public userRole: any;

  constructor(private authenticationService: AuthenticationService,
    private userService: UserService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x)
  }

  ngOnInit() {
    this.getProfile();
  }

  getProfile() {
    this.userService.getUser(this.currentUser.userID).subscribe(data => {
      console.log(data);
      this.userProfile = data;
      console.log("userProfile" + this.userProfile);
    });

    if (this.currentUser.role === 1) {
      this.userRole = 'Patient';
    } else if (this.currentUser.role === 2) {
      this.userRole = 'Doctor';
    } else if (this.currentUser.role === 3) {
      this.userRole = 'Administrator';
    }

  }
}
