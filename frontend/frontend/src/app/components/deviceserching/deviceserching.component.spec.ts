import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceserchingComponent } from './deviceserching.component';

describe('DeviceserchingComponent', () => {
  let component: DeviceserchingComponent;
  let fixture: ComponentFixture<DeviceserchingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceserchingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceserchingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
