import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { DeviceService } from '@app/_services/device.service';
import { Device } from '@app/_models';
import { getDefaultService } from 'selenium-webdriver/opera';
@Component({
  selector: 'app-deviceserching',
  templateUrl: './deviceserching.component.html',
  styleUrls: ['./deviceserching.component.css']
})
export class DeviceserchingComponent implements OnInit {
  DeviceserchingForm: FormGroup;
  DeviceAddForm: FormGroup;
  heroes : any = [];
  objPost : any;
  title = 'Devices Management';
  searchText;
  async delay(ms: number) {
    await new Promise(resolve => setTimeout(()=>resolve(), ms)).then(()=>console.log("fired"));
}
  
  constructor(private formBuilder: FormBuilder,  private deviceService: DeviceService) { }

  ngOnInit() {
       
    this.deviceService.getDevices()
    .subscribe(
      data =>
      {
      this.heroes = data;
      }
  ) 
  
    this.DeviceserchingForm = this.formBuilder.group({
      id: ['', Validators.required],
      deviceType: ['', Validators.required],
      producer: ['', Validators.required],
      model: ['', Validators.required],
      serialNumber: ['', Validators.required],
      others: ['', Validators.required],
      userID: ['', Validators.required]
    });

    this.DeviceAddForm = this.formBuilder.group({
      id: ['', Validators.required],
      deviceType: ['', Validators.required],
      producer: ['', Validators.required],
      model: ['', Validators.required],
      serialNumber: ['', Validators.required],
      others: ['', Validators.required],
      userID: ['', Validators.required]
    });
    
  }
  onDeleteClick(event,id){
    this.deviceService.deleteDevice(id.id).subscribe();
    this.delay(1500).then(any=>{
      window.location.reload();
  });
  }
  onEditClick(event,id) {
    function replacer(key, value) {
      if(value =='')
        return undefined;
      return value;
   }
   const obj = JSON.stringify(this.DeviceserchingForm.value,replacer);
   console.log(JSON.parse(obj));
   this.deviceService.patchDevices(id.id,obj).subscribe(
     data=>
     {
       this.objPost = data;
     }
   )
   this.delay(1500).then(any=>{
    window.location.reload();
});
   
    
  }

  onAddClick(){
   // console.log("cccc" + JSON.stringify(this.heroes ));
    function replacer2(key, value) {
      if(key ==='id')
        return undefined;
      if(key ==='userID')
        return undefined;
      return value;
   }
   const obj = JSON.stringify(this.DeviceAddForm.value,replacer2);
   console.log(obj);
   
   this.deviceService.postDevices(obj).subscribe(
     data=>
     {
        this.objPost = data;
     }
   )
   this.delay(1500).then(any=>{
    window.location.reload();
   });
  }

}
