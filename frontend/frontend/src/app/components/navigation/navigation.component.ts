import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../_services';
import { User, Role } from '../../_models';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  @ViewChild('sidenav') sidenav: ElementRef;

  @Output() measurementInputClick = new EventEmitter<boolean>();

  clicked: boolean;
  currentUser: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {

   
  
  }

  setClicked(val: boolean): void {
    this.clicked = val;
  }

  measurementInputOnClick() {
    this.measurementInputClick.emit(true);
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.Admin;
  }

  get isUser() {
    return this.currentUser && this.currentUser.role === Role.User;
  }

  get isDoctor() {
    return this.currentUser && this.currentUser.role === Role.Doctor;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
