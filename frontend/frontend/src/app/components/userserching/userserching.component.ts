import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { UserService} from '@app/_services/user.service' 
import { getDefaultService } from 'selenium-webdriver/opera';
import { User } from '@app/_models';

@Component({
  selector: 'app-userserching',
  templateUrl: './userserching.component.html',
  styleUrls: ['./userserching.component.css']
})
export class UserserchingComponent implements OnInit {
  UserserchingForm: FormGroup;
  title = 'Zarządzanie kontami';
  searchText;
  update: any = []
  heroes: any = []
  users: any = []
  doctors: any = []
  public selectedUser: number | "" = "";
  public selectedDoctor: number | "" = "";
  public selectedUser2: number | "" = "";
  public selectedDoctor2: number | "" = "";
  async delay(ms: number) {
    await new Promise(resolve => setTimeout(()=>resolve(), ms)).then(()=>console.log("fired"));
  }
  constructor(private formBuilder: FormBuilder,  private usereService: UserService) { }

  ngOnInit() {

    this.usereService.getAll().subscribe(
      data=>
      {
        this.heroes = data;
      }
       )

       this.usereService.getAll().subscribe(
        data=>
        {
          this.users = data;
        }
         )

    this.usereService.getDoctors().subscribe(
        data=>
        {
          this.doctors = data;
         
        }
    ) 
       
    this.UserserchingForm = this.formBuilder.group({
      user_id: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      first_name: ['', Validators.required],
      surname: ['', Validators.required],
      national_id: ['', Validators.required],
      telephone: ['', Validators.required],
      role_id: ['', Validators.required]
    });
  }
  selectedData(){
    if (this.selectedUser.toString() != "" && this.selectedDoctor.toString() != ""){
      this.usereService.setPatient(this.selectedUser, this.selectedDoctor).subscribe();
    }
  }
  deletePatient(){
    if (this.selectedUser2.toString() != "" && this.selectedDoctor2.toString() != ""){
      this.usereService.deletePatient(this.selectedUser2, this.selectedDoctor2).subscribe();
    }
  }
  onEditClick(event,id) {
  //  console.log("cccc" + JSON.stringify(this.heroes ));

    this.UserserchingForm.value.user_id = id.id;

     function replacer(key, value) {
     // console.log(typeof value);
      if (key === 'email') {
        if(value =='')
        return undefined;
      }
      if (key === 'password') {
        if(value =='')
        return undefined;
      }
      if (key === 'first_name') {
        if(value =='')
        return undefined;
      }
      if (key === 'surname') {
        if(value =='')
        return undefined;
      }
      if (key === 'national_id') {
        if(value =='')
        return undefined;
      }
      if (key === 'telephone') {
        if(value =='')
        return undefined;
      }
      if (key === 'role_id') {
        if(value =='')
        return undefined;
      }
      return value;
    }
    const obj = JSON.stringify(this.UserserchingForm.value,replacer);
    console.log(JSON.parse(obj));
    this.usereService.update(obj);   

    console.log(this.doctors);
    console.log("Users : " );
    console.log(this.heroes);
    this.delay(1500).then(any=>{
      window.location.reload();
  });
   }
}
