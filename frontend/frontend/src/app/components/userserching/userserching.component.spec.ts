import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserserchingComponent } from './userserching.component';

describe('UserserchingComponent', () => {
  let component: UserserchingComponent;
  let fixture: ComponentFixture<UserserchingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserserchingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserserchingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
