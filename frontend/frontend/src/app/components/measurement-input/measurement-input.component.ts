import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MeasurementsService } from '@app/_services';
import { catchError, map } from 'rxjs/operators';
import { AddMeasurement } from '@app/_models';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-measurement-input',
  templateUrl: './measurement-input.component.html',
  styleUrls: ['./measurement-input.component.css']
})
export class MeasurementInputComponent implements OnInit {
  public selectedName: number | "" = "";

  measurementForm: FormGroup;
  measurement: AddMeasurement;
  saveFlag: number = 0;
  saved = false;
  types: any = [];
  unit: any = '';
  //---------------------------------------------------//

  private subject = new Subject<any>();
  public response;

  constructor(private formBuilder: FormBuilder,
    private measurementsService: MeasurementsService,
    private http: Http) {

  }

  ngOnInit() {
    this.measurementForm = this.formBuilder.group({
      deviceID: ['', Validators.required],
      value: ['', Validators.required],
      description: ['', Validators.required]
    });
    this.saveFlag = 0;
    this.getTypes();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.measurementForm.controls;
  }

  onSaveClick() {
    this.saved = true;



    // stop here if form is invalid
    if (this.measurementForm.invalid) {
      return;
    }

    var date: any = '';

    if (this.saveFlag == 0) {
      this.measurement = {
        datetime: date,
        measurements: [{
          deviceID: this.f.deviceID.value, name: parseInt(this.selectedName.toString()),
          unit: parseInt(this.types[parseInt(this.selectedName.toString()) - 1].units[0].id), value: this.f.value.value, description: this.f.description.value
        }]
      }
    } else {
      this.measurement.measurements.push({
        deviceID: this.f.deviceID.value, name: parseInt(this.selectedName.toString()),
        unit: parseInt(this.types[parseInt(this.selectedName.toString()) - 1].units[0].id), value: this.f.value.value, description: this.f.description.value
      });
    }
    console.log("pomiar: " + JSON.stringify(this.measurement));
    console.log("name: " + parseInt(this.selectedName.toString()));
    console.log("unit: " + parseInt(this.types[parseInt(this.selectedName.toString()) - 1].units[0].id));

    this.saveFlag++;
    this.saved = false;
  }

  onSendClick() {
    console.log(this.measurementForm.value);
    var currentDate: Date = new Date(Date.now());
    console.log("currentDate:" + currentDate);
    var dateString: String = currentDate.toString();
    console.log("dateString:" + dateString);
    var splitted = currentDate.toString().split(" ", 5);
    var date = splitted[0] + " " + splitted[1] + " " + splitted[2] + " " + splitted[3] + " " + splitted[4];
    this.measurement.datetime = date;

    console.log("pomiar: " + JSON.stringify(this.measurement));

    this.measurementsService.setMeasurement(this.measurement)
      .subscribe(data => {
        console.log("daodanie do bazy" + JSON.stringify(data));
        this.response = data.response;
        console.log("RESPONSE" + this.response);
      });

    this.saveFlag = 0;
    this.measurement.datetime = '';
    this.measurement.measurements.splice(0);
  }

  getTypes() {
    this.http.get(`${environment.apiUrl}/application/measurements/types`)
      .pipe(map((res: Response) => res.json())).subscribe(types => {
        console.log(types);
        this.types = types;
      })
  }

  isSelectedName() {
    console.log("selected name:" + this.selectedName);

    if (this.selectedName != "") {
      this.unit = this.types[this.selectedName - 1].units[0].name;
      console.log("unit: " + this.types[this.selectedName - 1].units[0].name)
    }

  }

}
