import { Component, OnInit, Inject } from '@angular/core';
import { MeasurementsService } from '@app/_services';
import { Measurement } from '@app/_models';
import { first, map } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../../_services';
import { User, Role } from '../../_models';
import { DatePipe } from '@angular/common';
import { FormControl, Validators } from '@angular/forms';
import { CommentService } from '@app/_services/comment.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  commentFormModal = new FormControl('', Validators.required);
  measurementId: number;

  public selectedPatient: number | "" = "";
  public selectedType: number | "" = "";
  public selectedInterval: number | "" = "";

  public flag: boolean = false;
  public selectedPatientFlag: boolean = false;
  public commentFlag: boolean = false;
  /*----------------------------------------------*/
  numberOfDatasets: any;
  currentUser: User;
  patients: any[];
  results: any = [];
  types: any = [];
  comments: any = [];
  searchText: string = '';
  response: string;
  /*-----------------------------------------------*/
  public map: any = { lat: 51.678418, lng: 7.809007 };
  public chart1Type: string = 'bar';
  public chart2Type: string = 'pie';
  public chart3Type: string = 'line';
  public chart4Type: string = 'radar';
  public chart5Type: string = 'doughnut';
  public title: String = 'tytul';

  public chartType = 'line';

  public chartDatasets: Array<any> = [
    { data: [], label: '#1' },
  ];
  public chartLabels: Array<any> = [];

  public chartColors: Array<any> = [];

  public dateOptionsSelect: any[];
  public bulkOptionsSelect: any[];
  public showOnlyOptionsSelect: any[];
  public filterOptionsSelect: any[];

  public chartOptions: any = {
    responsive: true,
    legend: {
      labels: {
        fontColor: '#5b5f62',
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontColor: '#5b5f62',
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: '#5b5f62',
        }
      }]
    }
  };

  constructor(
    private measurementServiece: MeasurementsService,
    private http: Http,
    private authenticationService: AuthenticationService,
    private commentService: CommentService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
    this.flag = false;
    this.selectedPatientFlag = false;
    this.numberOfDatasets = 0;

    if (!this.isDoctor) {
      this.getTypesResults(this.currentUser.userID);
      this.getComments(this.currentUser.userID);
    } else {
      this.getPatients();
    }
  }

  get isDoctor() {
    return this.currentUser && this.currentUser.role === Role.Doctor;
  }

  getComments(userID) {
    this.commentService.getComments(parseInt(userID)).subscribe(comments => {
      console.log(comments);
      this.comments = comments;
    })
  }

  getTypesResults(userId) {
    this.http.get(`${environment.apiUrl}/application/measurements/` + userId)
      .pipe(map((res: Response) => res.json())).subscribe(types => {
        console.log(types);
        this.types = types;
      })
  }

  getPatients() {
    this.http.get(`${environment.apiUrl}/application/doctors/` + this.currentUser.userID)
      .pipe(map((res: Response) => res.json())).subscribe(patients => {
        this.patients = patients;
        console.log("patients: " + this.patients);
      })
  }

  getResults(patient, type, interval) {
    this.getData(patient, type, interval).subscribe(results => {
      console.log(results);
      this.results = results;
      this.parseData(results);
    })
  }

  getData(patient, type, interval) {
    var currentDate: Date = new Date(Date.now());
    console.log("Data:" + currentDate);
    var splitted = currentDate.toString().split(" ", 5);
    var dateEnd = splitted[0] + " " + splitted[1] + " " + splitted[2] + " " + splitted[3] + " " + splitted[4];

    var date: any = currentDate;
    date.setDate(date.getDate() - interval);
    console.log("Data:" + date);
    var splitted1 = date.toString().split(" ", 5);
    var dateStart = splitted1[0] + " " + splitted1[1] + " " + splitted1[2] + " " + splitted1[3] + " " + splitted1[4];

    console.log("dateStart " + dateEnd);
    console.log("dateEnd " + dateStart);

    return this.http.get(`${environment.apiUrl}/application/measurements/` + patient + "?type=" + type + "&date_from=" + dateStart + "&date_to=" + dateEnd)
      .pipe(map((res: Response) => res.json()))
  }

  parseData(results) {
    results.forEach(element => {
      console.log(element.value);
      console.log(element.typeName);

      var splitted = element.timestamp.split(" ", 6);
      this.chartLabels.push(splitted[1] + " " + splitted[2] + " " + splitted[3] + " " + splitted[4]);
      this.chartDatasets[0].data.push(element.value);
    });

    var title: any = results[0].typeName + " [" + results[0].unitName + "]";
    this.chartDatasets[0].label = title;
    console.log(results[0].unitName);
    this.title = title;

    console.log(title);
    console.log("labels:" + this.chartLabels);
    console.log(this.chartDatasets);
    this.flag = true;
  }

  selectedData() {
    if (this.selectedType.toString() != "" && this.selectedInterval.toString() != "") {
      this.flag = false;
      this.chartDatasets[0].data = [];
      this.chartLabels = [];
      if (!this.isDoctor) {
        this.getResults(this.currentUser.userID, this.selectedType, this.selectedInterval);
      } else {
        this.getResults(this.selectedPatient, this.selectedType, this.selectedInterval);
        this.getComments(this.selectedPatient);
      }
    }
  }

  onClickPatient() {
    this.selectedPatientFlag = true;
    this.getTypesResults(this.selectedPatient);
  }

  measurementIndex(id: number) {
    this.commentFlag = true;
    this.comments.forEach(element => {
      if (element.measurementID == id) {
        this.commentFlag = false;
        console.log("ten pomiar już ma komentarz");
      }
    });
    if (this.commentFlag) {
      this.measurementId = id;
    }
  }

  addComment() {
    console.log("add comment");
    console.log(this.commentFormModal.value);

    var currentDate: Date = new Date(Date.now());
    console.log("Data:" + currentDate);
    var splitted = currentDate.toString().split(" ", 5);
    var dateSend = splitted[0] + " " + splitted[1] + " " + splitted[2] + " " + splitted[3] + " " + splitted[4];

    var comment = {
      timestamp: dateSend,
      measurementID: this.measurementId,
      content: this.commentFormModal.value
    }
    this.commentService.postComment(parseInt(this.selectedPatient.toString()), comment).subscribe(data => {
      console.log("daodanie do bazy" + JSON.stringify(data));
      this.response = data.response;
      console.log("RESPONSE" + this.response);
    });
    this.commentFormModal.setValue("");
  }
}


