import { Component, OnInit } from '@angular/core';
import { Http, Response} from '@angular/http';
import { AuthenticationService } from '@app/_services';
import { User } from '@app/_models';
import { environment } from '@environments/environment';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {

  currentUser: User;
  public patients: any[];

  constructor( 
    private http: Http,
    private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x)
  }

  ngOnInit() {
    this.getPatients();
  }
  
  getPatients() {
    this.http.get(`${environment.apiUrl}/application/doctors/` + this.currentUser.userID)
      .pipe(map((res: Response) => res.json())).subscribe(patients => {
        this.patients = patients;
        console.log("patients: " + this.patients);
      })
  }
}
