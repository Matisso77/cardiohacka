export class Measurement {
    description: String;
    deviceID: number;
    id: number;
    timestamp: String;
    typeID: number;
    unitID: number;
    value: number; 
}


export class AddMeasurement {
  [x: string]: any;
    datetime: any;
    measurements: [
        {
        deviceID: number,
        name: number,
        unit: number,
        value: number,
        description: String,
    }];
  }
  