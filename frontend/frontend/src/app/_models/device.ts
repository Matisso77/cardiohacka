export class Device {
    id: number;
    deviceType: string;
	producer: string;
	model: string;
	serialNumber: string;
	others: string;
	userId: number;
}
