﻿﻿import { Role } from "./role";

export class User {
    userID: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    role: Role;
    access_token: string;
}
