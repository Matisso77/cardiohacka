import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtInterceptor } from '@app/_helpers/jwt.interceptor' ;
import { environment } from '@environments/environment';
import { Device } from '@app/_models';
import { Observable } from 'rxjs';



@Injectable({ providedIn: 'root' })
export class DeviceService {

    constructor(private http: HttpClient) { }
    getDevices(): Observable<any> {
        return this.http.get(`${environment.apiUrl}/application/devices/`);
    }
    postDevices(device : any): Observable<any>{
        return this.http.post(`${environment.apiUrl}/application/devices/`,device);//.subscribe(data =>{console.log(JSON.stringify(data));});
    }
    patchDevices(id : number, upData : any){
        return this.http.patch(`${environment.apiUrl}/application/devices/${id}`,upData);//.subscribe(data =>{console.log(JSON.stringify(data));});
    }
    deleteDevice(id : number){
        return this.http.delete(`${environment.apiUrl}/application/devices/${id}`);
    }
}
