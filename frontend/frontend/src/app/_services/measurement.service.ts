import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '@environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Measurement, AddMeasurement, User } from '@app/_models';
import { throwError } from 'rxjs';


 const httpOptions = {
    headers: new HttpHeaders({
    })
  };

@Injectable({
  providedIn: 'root'
})
export class MeasurementsService {

  currentUser: User;
 
  constructor(private http: HttpClient) { 
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
  }

  setMeasurement(measurement : AddMeasurement) {
  
  try {
    console.log("przed" + this.currentUser.access_token)
    var request: any = this.http.post<any>(`${environment.apiUrl}/application/measurements/add`,JSON.stringify(measurement),httpOptions);
    console.log("po")
    return request;

  } catch (error) {
    console.log("sssssssssssssss" + error);
    return null;
  }
 
  }

  getMeasurement(User_id: number, measurements_type_id: number, data_form: string, data_to: string) {

    return this.http.get<Measurement[]>(`${environment.apiUrl}/application/measurements/${User_id}?type=${measurements_type_id}&date_from=${data_form}.123456Z&date_to=${data_to}.123456Z`);
  }

  getMeasurementType(User_id: number) {
    return this.http.get<string[]>(`${environment.apiUrl}/aplication/measurements/${User_id}`);
  }

 private handleError(error: HttpErrorResponse) {
   console.log("errorHandle");
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.error}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
