﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtInterceptor } from '@app/_helpers/jwt.interceptor' ;
import { environment } from '@environments/environment';
import { User } from '@app/_models';
import { Observable } from 'rxjs';


const httpOptions = {
    headers: new HttpHeaders({
    })
  };
  
@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll(): Observable<any> {
        return this.http.get(`${environment.apiUrl}/application/users`);//  .subscribe(data =>{console.log(JSON.stringify(data));});
    }

    getUser(id: number) {
        return this.http.get(`${environment.apiUrl}/application/users/${id}`);
    }
        getDoctors(): Observable<any> {
        return this.http.get(`${environment.apiUrl}/application/doctors/`);//.subscribe(data =>{console.log(JSON.stringify(data));});
    }

    setPatient(patientId , doctorId ): Observable<any>{
        return this.http.post(`${environment.apiUrl}/application/doctors/${doctorId}/patient/${patientId}`, null);//.subscribe(data=>{console.log(data)});
    }

    deletePatient(patientId , doctorId ): Observable<any>{
        return this.http.delete(`${environment.apiUrl}/application/doctors/${doctorId}/patient/${patientId}`);//.subscribe(data=>{console.log(data)});
    }

    register(user: User) {
        console.log("user: "+ User);
        return this.http.post<any>(`${environment.apiUrl}/application/auth/register?Content-Type=application/json`,user);
    }
    
    update(user: any) {
        console.log("send?");
        return this.http.patch(`${environment.apiUrl}/application/users`, user).subscribe(data =>{console.log(JSON.stringify(data));});
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/application/users/${id}`);
    }
}