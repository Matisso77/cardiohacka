import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtInterceptor } from '@app/_helpers/jwt.interceptor';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  constructor(private http: HttpClient) { }

  getComments(userId: number) {
    return this.http.get(`${environment.apiUrl}/application/comments/user/${userId}`);
  }

  postComment(userId: any, comment: any) {
    return this.http.post<any>(`${environment.apiUrl}/application/comments/user/${userId}`, 
    JSON.stringify(comment));
  }

  patchComment(commentId: number, comment: any) {

    return this.http.patch<any>(`${environment.apiUrl}/application/comments/${commentId}`, comment);
  }

  deleteComment(commentId: number) {
    return this.http.delete(`${environment.apiUrl}/application/comments/${commentId}`);
  }

}