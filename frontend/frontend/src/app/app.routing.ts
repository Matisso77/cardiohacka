﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './components/login';
import { RegisterComponent } from './components/register';
import { AuthGuard } from './_guards';
import { Role } from './_models';
import { MeasurementInputComponent } from './components/measurement-input';
import { ResultsComponent } from './components/results/results.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PatientsComponent } from './components/patients/patients.component';
import { UserserchingComponent } from './components/userserching/userserching.component';
import { DeviceserchingComponent} from './components/deviceserching/deviceserching.component';

const appRoutes: Routes = [
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'accounts', component:UserserchingComponent,  canActivate: [AuthGuard] },
    { path: 'devices', component:DeviceserchingComponent,  canActivate: [AuthGuard] },
    { path: 'register', component: RegisterComponent },
    { path: 'measurement_input', component: MeasurementInputComponent,  canActivate: [AuthGuard]},
    { path: 'results', component: ResultsComponent,  canActivate: [AuthGuard]},
    { path: 'profile', component: ProfileComponent,  canActivate: [AuthGuard]},
    { path: 'patients', component: PatientsComponent,  canActivate: [AuthGuard]},

    // otherwise redirect to home
    { path: '**', redirectTo: 'home' }
];

export const routing = RouterModule.forRoot(appRoutes);