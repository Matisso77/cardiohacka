﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { JwtModule } from '@auth0/angular-jwt';

//search
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { AlertComponent } from './_services/_components';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { LoginComponent } from './components/login';
import { RegisterComponent } from './components/register';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NavigationComponent } from './components/navigation/navigation.component';
import { MeasurementInputComponent } from './components/measurement-input/measurement-input.component';
import { ResultsComponent } from './components/results/results.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PatientsComponent } from './components/patients/patients.component';
import { UserserchingComponent } from './components/userserching/userserching.component';
import { DeviceserchingComponent } from './components/deviceserching/deviceserching.component';

@NgModule({
    imports: [
        JwtModule.forRoot({
            config: {
            tokenGetter: function  tokenGetter() {
                return localStorage.getItem('access_token');},
            whitelistedDomains: ['localhost:3000'],
            blacklistedRoutes: ['http://localhost:3000/auth/login']
          }
        }),
        BrowserModule,
        FormsModule,
        Ng2SearchPipeModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpModule,
        routing,
        MDBBootstrapModule.forRoot(),
        MDBBootstrapModule],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        NavigationComponent,
        NavigationComponent,
        MeasurementInputComponent,
        ResultsComponent,
        ProfileComponent,
        UserserchingComponent,
        PatientsComponent,
        DeviceserchingComponent ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }